#!/bin/sh
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -notify
#$ -P gentech-rqc.p
#$ -j y -o imgap_worker_$TASK_ID.log
#$ -l normal.c
#$ -N imgap_212186
#$ -l h_rt=29:59:59
#$ -pe pe_slots 32
module load python
module unload tfmq
module load tfmq
for i in {1..1}
do
    tfmq-worker -q imgap &
done
wait
