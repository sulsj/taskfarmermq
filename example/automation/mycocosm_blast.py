#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Mycocosm Blast pipeline



* Way to get the reference db list
----------------------------------
#!/usr/bin/env python
import json
import requests
data = requests.get('http://genome.jgi.doe.gov/ext-api/mycocosm/catalog/all-portals').json()
for items in data:
    if 'blastable' in items:
        for entry in items['blastable']:
            if items['blastable'][entry]['category']=='GeneModels--Transcripts':
                print items['blastable'][entry]['fastaDb']


$ python tools/get_db.py > ref_db_list.txt



* TFMQ task example
--------------------
module unload blast+; module load blast+/2.6.0; blastn -query /global/projectb/scratch/sulsj/2017.02.24-mycocosm-bla
st/10898.8.183776.ACTTGA.fasta -db /global/dna/shared/databases/Aaoar/Aaoar1/Aaoar1_GeneModels_AllModels_20140429_nt
.fasta -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_ali
gnments 5 -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitle
s' -num_threads 8 > /global/projectb/scratch/sulsj/2017.02.24-mycocosm-blast/tfmq_test/out/10898.8.183776.ACTTGA.fas
ta.vs.Aaoar1_GeneModels_AllModels_20140429_nt.fasta.bout 2> /global/projectb/scratch/sulsj/2017.02.24-mycocosm-blast
/tfmq_test/out/10898.8.183776.ACTTGA.fasta.vs.Aaoar1_GeneModels_AllModels_20140429_nt.fasta.bout.log::0




* TFMQ Blast worker qsub script
-------------------------------
#!/bin/sh

#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -notify
#$ -P gentech-rqc.p
#$ -j y -o <log_file>_$TASK_ID.log
#$ -l normal.c
#$ -N tfmq_mycocosm_blast_<job_name>
#$ -l h_rt=12:00:00
#$ -pe pe_slots 32

module load python
module unload tfmq
module load tfmq

for i in {1..4}
do
    ## wait 60sec for client, allow empty output files
    tfmq-worker -q <queue_name> -t 60 -z -ld <worker_log_dir> &
done
wait

##
## To start the client,
## module load tfmq
## tfmq-client -i task_list.tfmq -q <seq_unit_name>_blastmq
##



Created: Mar 17 2017

sulsj (ssul@lbl.gov)


Revisions:

    03172017 0.0.9: init
    03302017 0.0.9: Changed evalue to 1e-05
    06052017 0.1.0: Added last aligner
    07052017 0.2.0: Added diamond aligner

    07172017 1.0.0: Ready for production

    10172017 1.1.0: Changed to Last aligner
    
    05012018 1.2.0: Changed to Diamond

"""

import os
import sys
# import time
import argparse
import uuid
# from tempfile import mkstemp

## custom libs in "../lib/"
SRCDIR = os.path.dirname(__file__)
sys.path.append(os.path.join(SRCDIR, '../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(SRCDIR, '../tools'))  ## rqc-pipeline/tools

from common import get_logger, get_status, append_rqc_stats, append_rqc_file, get_colors
from rqc_utility import *
from rqc_constants import *
from db_access import *
from os_utility import make_dir, change_mod, run_sh_command

SCRIPTNAME = __file__
VERSION = "1.1.0"
#LOGLEVEL = "INFO"
LOGLEVEL = "DEBUG"
CFG = {}

color = get_colors()
SUCCESS = 0
FAILURE = -1


"""
STEP 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Collect blaststable databases by
requests.get('http://genome.jgi.doe.gov/ext-api/mycocosm/catalog/all-portals').json()


"""
def collect_blast_dbs(refList, outputPath, log):
    log.info("\n\n%sSTEP1 - Collect blastable mycocosm databases <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "1_collect_blast_dbs in progress"
    checkpoint_step_wrapper(status)

    statsFile = CFG["stats_file"]

    referenceDir = "blastdb"
    referencePath = os.path.join(outputPath, referenceDir)
    make_dir(referencePath)
    change_mod(referencePath, "0755")
    dbListFile = os.path.join(referencePath, "db.list")


    if not refList:
        cmd = "%s/tools/get_db.py > %s" % (SRCDIR, dbListFile)
        _, _, exitCode = run_sh_command(cmd, True, log, True)
    else:
        cpCmd = "cp %s %s" % (refList, dbListFile)
        _, _, exitCode = run_sh_command(cpCmd, True, log, True)
        assert exitCode == SUCCESS


    if exitCode != SUCCESS:
        status = "1_collect_blast_dbs failed"
        log.info(status)
        checkpoint_step_wrapper(status)

    else:
        status = "1_collect_blast_dbs complete"
        log.info(status)
        checkpoint_step_wrapper(status)

        assert os.path.isfile(dbListFile)
        cmd = "wc -l %s" % (dbListFile)
        stdOut, _, _ = run_sh_command(cmd, True, log, True)
        log.info("Blast referece database list file successfully created: %s", dbListFile)
        log.info("Number of Mycocosm databases: %s", stdOut)
        append_rqc_stats(statsFile, "number of blastable dbs collected", stdOut.split()[0], log)
        append_rqc_file(filesFile, "blast databases collected", dbListFile, log)

    return status, dbListFile


"""
STEP 2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def create_tfmq_task_list_file(query, dblist, outputPath, log):
    log.info("\n\n%sSTEP2 - Create tfmq task list input file <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "2_create_tfmq_task_list_file in progress"
    checkpoint_step_wrapper(status)

    filesFile = CFG["files_file"]

    tfmqTaskDir = "tfmqtask"
    tfmqTaskPath = os.path.join(outputPath, tfmqTaskDir)
    make_dir(tfmqTaskPath)
    change_mod(tfmqTaskPath, "0755")
    tfmqTaskListFile = os.path.join(tfmqTaskPath, safe_basename(query, log)[0] + ".tfmq")

    blastOutDir = "blastout"
    blastOutPath = os.path.join(outputPath, blastOutDir)
    make_dir(blastOutPath)
    change_mod(blastOutPath, "0755")

    TFMQ_TEMPLATE = "%s::0"

    ## BLAST+

    # BLASTN_TEMPLATE = "module unload blast+; module load blast+/2.6.0; blastn -query %s -db %s %s > %s 2> %s.log"
    # BLASTX_TEMPLATE = "module unload blast+; module load blast+/2.6.0; blastx -query %s -db %s %s > %s 2> %s.log"

    ## Last
    ## cmd = "module load last; lastal -F15 -P0 -f BlastTab %s %s > %s 2> %s" % (nr_db, fasta, last_output, last_log)
    ## -F15 = framshift of 15, -P0 = use all threads
    ## run_last.py -p '-F15 -b1 -x15 -y7 -z25 -m1 ' mycoProt input_assembly.fasta -o output_results.maf -all
    # LAST_TEMPLATE = "module unload samtools; module load samtools/20130508; module load jigsaw; run_last.py -p ' -F15 -b1 -x15 -y7 -z25 -m1 ' %s %s -o %s -all 2> %s.log"
    LAST_TEMPLATE = "set -e; module load last; lastdb -p -cR01 %s %s; \
module unload samtools; module load samtools/20130508; module load jigsaw; run_last.py -p ' %s ' %s %s -o %s -all 2> %s.log; \
rm -rf %s.*"

    ## Diamond
    ## Ref) https://ab.inf.uni-tuebingen.de/software/diamond/
    ##      https://github.com/bbuchfink/diamond
    ## --top is recommended than -k
    # DIAMOND_TEMPLATE = "/global/projectb/scratch/sulsj/2017.07.05-diamond-test/diamond blastx -q %s -d %s -e 0.00001 -o %s -p 8 --top 10 --comrpess 1 2> %s.log"
    # DIAMOND_TEMPLATE = "/global/projectb/scratch/sulsj/2017.07.05-diamond-test/diamond blastx -q %s -d %s -o %s %s 2> %s.log"
    # DIAMOND_TEMPLATE = "/global/dna/projectdirs/PI/rqc/prod/tools/diamond/diamond makedb --in %s --db %s && \
    #                       /global/dna/projectdirs/PI/rqc/prod/tools/diamond/diamond blastx -q %s -d %s -o %s %s 2> %s.log && \
    #                       rm -rf %s && \
    #                       cat %s | sort -k3,3rn -k4,4g -k6,6rn -uk1,1 | head -n 1 > %s"

    DIAMOND_TEMPLATE = "/global/dna/projectdirs/PI/rqc/prod/tools/diamond/diamond makedb --in %s --db %s && \
/global/dna/projectdirs/PI/rqc/prod/tools/diamond/diamond blastx -q %s -d %s -o %s %s 2> %s.log && \
rm -rf %s"


    if dblist is None: ## when restarted in step 2
        referenceDir = "blastdb"
        referencePath = os.path.join(outputPath, referenceDir)
        dbFile = os.path.join(referencePath, "db.list")
        if os.path.isfile(dbFile):
            dblist = dbFile
        else:
            log.info("2_create_tfmq_task_list_file failed. Reference database list file doesn't exist.")
            status = "2_create_tfmq_task_list_file failed"
            checkpoint_step_wrapper(status)
            return status, None, None


    numTasks = 0
    with open(dblist, 'r') as DLFH:
        with open(tfmqTaskListFile, 'w') as tfmqFH:
            for l in DLFH:
                l = l.strip()
                blastoutFileName = safe_basename(query, log)[0] + ".vs." + safe_basename(l, log)[0] + ".bout"
                topOneFileName = safe_basename(query, log)[0] + ".vs." + safe_basename(l, log)[0] + ".bout.top1"
                outFile = os.path.join(blastOutPath, blastoutFileName)
                # topOneFiles = os.path.join(blastOutPath, topOneFileName)

                # blastxCmd = BLASTX_TEMPLATE % (query, l.strip(), CFG["blastx_param"], outFiles, outFiles)
                # lastCmd = LAST_TEMPLATE % (l.strip(), query, outFiles, outFiles)
                # diamondCmd = DIAMOND_TEMPLATE % (query, l.strip(), CFG["blastx_param"], outFiles, outFiles)

                # _, tempDbFile = mkstemp()
                tempDbFile = os.path.join("/scratch/tmp", safe_basename(l, log)[0])
                log.debug("Temp ref db name: %s" % (tempDbFile))

                # lastCmd = LAST_TEMPLATE % (tempDbFile, l, CFG["last_param"], tempDbFile, query, outFile, outFile, tempDbFile)
                diamondCmd = DIAMOND_TEMPLATE % (l, tempDbFile, query, tempDbFile, outFile, CFG["diamond_param"], outFile, tempDbFile)

                # tfmqCmd = TFMQ_TEMPLATE % (lastCmd)
                tfmqCmd = TFMQ_TEMPLATE % (diamondCmd)

                tfmqFH.write(tfmqCmd + '\n')
                numTasks += 1


    if not os.path.isfile(tfmqTaskListFile):
        status = "2_create_tfmq_task_list_file failed"
        log.info(status)
        checkpoint_step_wrapper(status)

    else:
        status = "2_create_tfmq_task_list_file complete"
        log.info(status)
        checkpoint_step_wrapper(status)

        log.info("TFMQ task file successfully created: %s", tfmqTaskListFile)
        log.info("Number of TFMQ task(s): %d", numTasks)
        append_rqc_file(filesFile, "tfmq input task list file", tfmqTaskListFile, log)
        append_rqc_file(filesFile, "blast output directory", blastOutPath, log)


    return status, tfmqTaskListFile, numTasks



"""
STEP 3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def tfmq_run(query, tfmqTaskFile, ntasks, outputPath, log):
    log.info("\n\n%sSTEP3 - Run tfmq <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "3_tfmq_run in progress"
    checkpoint_step_wrapper(status)

    filesFile = CFG["files_file"]

    qsubWorkingDir = "qsubscript"
    qsubWdPath = os.path.join(outputPath, qsubWorkingDir)
    make_dir(qsubWdPath)
    change_mod(qsubWdPath, "0755")

    if tfmqTaskFile is None: ## when restarted in step 3
        tfmqTaskDir = "tfmqtask"
        tfmqTaskPath = os.path.join(outputPath, tfmqTaskDir)
        tfmqFile = os.path.join(tfmqTaskPath, safe_basename(query, log)[0] + ".tfmq")
        if os.path.isfile(tfmqFile + ".done"):
            tfmqTaskFile = tfmqFile + ".done"
        elif os.path.isfile(tfmqFile):
            tfmqTaskFile = tfmqFile
        else:
            log.info("3_tfmq_run failed. Tfmq task list file doesn't exist.")
            status = "3_tfmq_run failed"
            checkpoint_step_wrapper(status)
            return status

    ## ----------------------------------------------
    ## Start workers (tfmq-worker)
    ## We will request 32core compute nodes and
    ## run 4 blastn processes on each node
    ## with num_threads=8
    ##
    ## A Diamond process will run in 32-thread mode (autodetect)
    ## per a compute node
    ## ----------------------------------------------
    # qsubFile = os.path.join(qsubWdPath, "worker.q")
    qsubFile = os.path.join(qsubWdPath, "worker_denovo.q")
    qsubLogFile = os.path.join(qsubWdPath, "tfmq_worker")
    taskFileName = safe_basename(tfmqTaskFile, log)[0]
    tfmqQname = '%s_%s' % (taskFileName, uuid.uuid4())

    # with open(os.path.join(SRCDIR, "template/worker.q"), 'r') as qfh:
    with open(os.path.join(SRCDIR, "template/worker_denovo.q"), 'r') as qfh:
        with open(qsubFile, 'w') as outfh:
            for l in qfh:
                outfh.write(l.replace("<queue_name>", tfmqQname)\
                             .replace("<log_file>", qsubLogFile)\
                             .replace("<job_name>", taskFileName)\
                             .replace("<worker_log_dir>", qsubWdPath))

    assert os.path.isfile(qsubFile)

    ## TODO
    ## which one has shorter wait time?
    # jidList = []
    # for i in range(1, CFG['num_nodes']):
    #     qsubCmd = "export TASK_ID=%d; qsub -v TASK_ID=$TASK_ID %s" % (i, qsubFile)
    #     stdOut, _, exitCode = run_sh_command(qsubCmd, True, log, True)
    #     assert exitCode == SUCCESS
    #     log.info("tfmq worker %d job info: %s", i, stdOut)
    #     jidList.append(stdOut.split()[2])

    # qsubCmd = "qsub -t 1-%s %s" % (CFG['num_nodes'], qsubFile)
    qsubCmd = "sbatch --array=1-%s %s" % (CFG['num_nodes'], qsubFile)
    stdOut, _, exitCode = run_sh_command(qsubCmd, True, log, True)
    assert exitCode == SUCCESS
    log.info("tfmq worker job info: %s", stdOut)

    ## ----------------------------------------------
    ## Start client (tfmq-client)
    ## ----------------------------------------------
    ## TODO
    ## Do we need timeout for tfmq-client?
    # tfqmClientCmd = "module load tfmq; timeout -v -t 21600s tfmq-client -i %s -q %s" % (tfmqTaskFile, tfmqQname)
    # tfqmClientCmd = "module unload tfmq; module load tfmq; tfmq-client -i %s -q %s -ld %s -d" % (tfmqTaskFile, tfmqQname, qsubWdPath)
    tfqmClientCmd = "/global/homes/s/sulsj/work/bitbucket-repo/taskfarmermq/tfmq-client -i %s -q %s -ld %s -d" % (tfmqTaskFile, tfmqQname, qsubWdPath)

    stdOut, stdErr, exitCode = run_sh_command(tfqmClientCmd, True, log, True)
    assert exitCode == SUCCESS, "Error in running tfmq-client: %s. %s" % (stdOut, stdErr)

    log.info('-' * 80)
    log.info("TFMQ-CLIENT: %s", stdOut)
    log.info('-' * 80)

    ## ----------------------------------------------
    ## Check all the tasks are completed
    ## ----------------------------------------------
    assert os.path.join(tfmqTaskFile + ".done")
    wcCmd = """grep "::1" %s | wc -l""" % (tfmqTaskFile + ".done")

    stdOut, _, exitCode = run_sh_command(wcCmd, True, log, True)
    assert exitCode == SUCCESS

    if int(stdOut.strip()) != ntasks:
        log.error("Not all tasks are successfully completed.")
        exitCode = FAILURE

    if exitCode != SUCCESS:
        status = "3_tfmq_run failed"
        log.info(status)
        checkpoint_step_wrapper(status)
    else:
        status = "3_tfmq_run complete"
        log.info(status)
        checkpoint_step_wrapper(status)

    append_rqc_file(filesFile, "tfmq qsub file", qsubFile, log)
    append_rqc_file(filesFile, "tfmq task done file", tfmqTaskFile + ".done", log)
    # append_rqc_file(filesFile, "tfmq worker qsub log file", qsubLogFile, log)
    # append_rqc_stats(statsFile, "tfmq worker job id(s)", ','.join(jidList), log)


    return status



"""
STEP 4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def post_processing(outputPath, log):
    log.info("\n\n%sSTEP4 - Post-processing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "4_post_processing in progress"
    checkpoint_step_wrapper(status)



    log.info("4_post_processing complete.")
    status = "4_post_processing complete"
    checkpoint_step_wrapper(status)

    return status




"""
STEP 5 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def cleanup(outputPath, log):
    log.info("\n\n%sSTEP5 - Cleanup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "5_cleanup in progress"
    checkpoint_step_wrapper(status)


    ## TODO
    ## cleanup
    ## blast log under blastout
    ## empty blast hit files under blastout
    ## tfmq_worker_&*.log under qsubscript ?
    ## logs dir under qsubscript ?


    status = "5_cleanup complete"
    log.info(status)
    checkpoint_step_wrapper(status)

    return status




'''===========================================================================
    checkpoint_step_wrapper

'''
def checkpoint_step_wrapper(status):
    assert(CFG["status_file"])
    checkpoint_step(CFG["status_file"], status)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

if __name__ == "__main__":
    desc = "RQC Mycocosm Blast Pipeline"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-d", "--db-list", dest="refList", help="Set reference data list file (default: ref_db_list.txt)", required=False)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Output path to write to", required=True)
    parser.add_argument("-p", "--num-nodes", dest="numComputeNodes", help="Number of compute nodes to request (default: 50)", default=50, required=False)
    parser.add_argument("-q", "--query", dest="inputQuery", help="Set input fasta/fastq file", required=True)
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    options = parser.parse_args()

    outputPath = options.outputPath
    inputQuery = options.inputQuery
    refList = options.refList
    numComputeNodes = options.numComputeNodes

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    CFG["status_file"] = os.path.join(outputPath, "mycocosm_blast_status.log")
    CFG["files_file"] = os.path.join(outputPath, "mycocosm_blast_files.tmp")
    CFG["stats_file"] = os.path.join(outputPath, "mycocosm_blast_stats.tmp")
    CFG["log_file"] = os.path.join(outputPath, "mycocosm_blast.log")
    # CFG["blastx_param"] = "-evalue 1e-05 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_alignments 5 -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' -num_threads 8"
    # CFG["blastx_param"] = "-evalue 1e-05 -task blastx -show_gis -num_alignments 5 -outfmt 6 -num_threads 8"
    # CFG["diamond_param"] = "-e 0.000001 --top 0 --outfmt 6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles"

    ## Sort by bit score (desc) and evalue (asc) and %id (desc) and query_id
    ## sort -k3,3rn -k4,4g -k6,6rn -uk1,1
    ##                                                                      1     2       3       4      5       6      7    8     9    10    11   12
    # CFG["diamond_param"] = "-e 0.000001 --top 0 --sensitive --outfmt 6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen"
    CFG["diamond_param"] = "-e 0.000001 --max-target-seqs 1 --outfmt 6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen"

    ## Last
    ## module unload samtools;
    ## module load samtools/20130508;
    ## module load jigsaw;

    ## Last example in sag.py
    ## EnvironmentModules.module(["load", config['jigsaw_module']])
    ## last_maf = os.path.join(contig_last_path, "last.maf")
    ## last_log = os.path.join(contig_last_path, "last.log")
    ## cmd = "run_last.py -p ' %s' -od %s -o %s %s %s > %s 2>&1" % (config['run_last_params'], contig_last_path, last_maf, config['nr_db'], contig_gene_fasta, last_log)
    ## std_out, std_err, exit_code = run_command(cmd, True)
    ## EnvironmentModules.module(["unload", config['jigsaw_module']])

    ############################################################################
    ## NOTE on Last
    ##
    ## Ref) http://last.cbrc.jp/doc/last-tutorial.html
    ##      http://last.cbrc.jp/doc/lastal.html
    ##      http://last.cbrc.jp/doc/last-evalues.html
    ##      https://groups.google.com/forum/#!topic/last-align/4VkCrNdd6jY
    ##      https://groups.google.com/forum/#!topic/last-align/8HFUEyp5dNQ
    ##
    ## Thresholds in Last
    ##   -D: report alignments that are expected by chance at most once per (say) thousand query letters (default: 1M)
    ##       or query letters per random alignment (1e+06)
    ##       ** This option only affects the default value of -E, so if you specify -E then -D has no effect.
    ##   -E: maximum expected alignments per square giga (1e+18/D/refSize/numOfStrands)
    ##       ** This option only affects the default value of -e, so if you specify -e then -E has no effect.
    ##
    ##   EG2 is "expected alignments per square giga". This is the expected number of alignments with greater or equal score, between two randomly-shuffled sequences of length 1 billion each.
    ##   E is the expected number of alignments with greater or equal score, between: a random sequence with the same length as the query sequence, and a random sequence with the same length as the database.
    ##
    ##   -F15: frameshift cost (off)
    ##   -b: gap extension cost (DNA: 1, protein: 2, 0<Q<5: 9)
    ##   -x: maximum score drop for gapped alignments (max[y, e-1])
    ##   -y: maximum score drop for gapless alignments (t*10)
    ##   -z: maximum score drop for final gapped alignments (x)
    ##   -m: maximum initial matches per query position (10)
    ##   -P0 = use all threads
    ##
    ## Speedup vs. Sensitivity (ref. http://last.cbrc.jp/doc/last-tuning.html)
    ##
    ##   Default: a=11 b=2 A=11 B=2 e=82 d=55 x=81 y=31 z=81 D=1e+06 E=1.65379e+06 R=01 u=2 s=1 S=0 M=0 T=0 m=10 l=1 n=10 k=1 w=1000 t=3.08611 j=3 Q=0
    ##
    ##   lastal
    ##     -k: initial matches skipping. -k2 or -k3 makes it faster.
    ##     -m: default 10, "-m1" makes it faster but less sensitivity.
    ##     -l: default 1, "-l50" makes it faster but less sensitivity.
    ##     -C: gapless alignment culling. ("-C2") It makes lastal faster but less sensitive. It can also reduce redundant output.
    ##     -x: Lower values make it faster, by quitting unpromising extensions sooner. Percent value.
    ##     -M: faster but cruder than standard gapped alignment. Default is False.
    ##     -j1: default 3. requests gapless alignments. -j1 is faster because it skips the gapping phase entirely.
    ##
    ##   lastdb (lastdb -p -cR01)
    ##     -w: indexing skipping
    ##     -W: check for initial matches starting at only some positions, in both query and reference
    ##     -i: This option makes lastdb faster, but disables some lastal options. If lastdb is too slow, try -i10.
    ############################################################################

    # CFG["last_param"] = "-F15 -b1 -x15 -y7 -z25 -m1"
    CFG["last_param"] = "-E0.000001 -F15 -b1 -x15 -y7 -z25 -m1 -P0"

    ## Number of compute nodes to request, will have 4 blastn processes per node ex) 25 nodes * 4 blast process = 100 blast processes
    ## ex) 428MB query file blasting against 982 reference dbs with 100 blast processes on 25 compute nodes
    ##     ==> took about 3hr to complete
    ##
    CFG['num_nodes'] = numComputeNodes

    print "Starting Mycocosm blast pipeline, writing log to: %s" % (CFG["log_file"])

    log = get_logger("mycocosm_blast", CFG["log_file"], LOGLEVEL)

    log.info("=================================================================")
    log.info("   Mycocosm Blast (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s...", SCRIPTNAME)


    status = None
    nextStep = 0


    if os.path.isfile(inputQuery):
        log.info("Found input query file, %s. Starting processing.", inputQuery)
        if os.path.isfile(CFG["status_file"]):
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s", status)

            if status == "start":
                pass

            elif status != "complete":
                nextStep = int(status.split("_")[0])
                if status.find("complete") != -1:
                    nextStep += 1
                log.info("Next step to do = %s", nextStep)

            else:
                ## already Done. just exit
                log.info("Completed %s: %s", SCRIPTNAME, inputQuery)
                exit(0)
        else:
            checkpoint_step_wrapper("start")
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s", status)

    else:
        log.error("- %s not found, aborting!", inputQuery)
        exit(2)



    done = False
    cycle = 0
    dbListFile = None
    tfmqTaskListFile = None
    nTasks = 0

    filesFile = CFG["files_file"]
    statsFile = CFG["stats_file"]


    if status == "complete":
        log.info("Status is complete")
        done = True


    while not done:
        cycle += 1

        if nextStep == 1 or status == "start":
            status, dbListFile = collect_blast_dbs(refList, outputPath, log)

        if status == "1_collect_blast_dbs failed":
            done = True


        if nextStep == 2 or status == "1_collect_blast_dbs complete":
            status, tfmqTaskListFile, nTasks = create_tfmq_task_list_file(inputQuery, dbListFile, outputPath, log)

        if status == "2_create_tfmq_task_list_file failed":
            done = True


        if nextStep == 3 or status == "2_create_tfmq_task_list_file complete":
            status = tfmq_run(inputQuery, tfmqTaskListFile, nTasks, outputPath, log)

        if status == "3_tfmq_run failed":
            done = True


        if nextStep == 4 or status == "3_tfmq_run complete":
            status = post_processing(outputPath, log)

        if status == "4_post_processing failed":
            done = True


        if nextStep == 5 or status == "4_post_processing complete":
            status = cleanup(outputPath, log)

        if status == "5_cleanup failed":
            done = True


        if status == "5_cleanup complete":
            status = "complete" ## FINAL COMPLETE!
            done = True

            ## move rqc-files.tmp to rqc-files.txt
            newFilesFile = os.path.join(outputPath, "mycocosm_blast_files.txt")
            newStatsFile = os.path.join(outputPath, "mycocosm_blast_stats.txt")

            cmd = "mv %s %s " % (CFG["files_file"], newFilesFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            cmd = "mv %s %s " % (CFG["stats_file"], newStatsFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)


        ## don't cycle more than 10 times ...
        if cycle > 10:
            done = True


    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\n\nCompleted %s", SCRIPTNAME)
        checkpoint_step_wrapper("complete")
        log.info("Done.")
        print "Done."


## EOF
