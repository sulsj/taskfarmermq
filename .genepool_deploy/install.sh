#!/bin/bash -l

set -ex

rsync -a --delete --exclude '.*' ./ $PREFIX/
cp .genepool_deploy/module_dependencies $PREFIX/.deps

